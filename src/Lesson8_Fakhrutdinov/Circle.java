package Lesson8_Fakhrutdinov;

public class Circle extends Ellipse {
    public Circle(Point center, double side1) {
        super(center, side1, side1);
    }

    public Circle(double side1) {
        super(side1, side1);
    }
}
