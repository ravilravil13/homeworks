package Lesson8_Fakhrutdinov;


public interface Relocatable {
    void move(double x, double y);

    void move(Point point);
}
