package Lesson6_Fakhrutdinov;

import java.util.Random;

public class Channel {
    private final int MAX_PROGRAM_NUMBER = 3;
    //Название канала
    private String nameChannel;
    //Массив программ
    private Program[] program;
    private int countProgram;
    private int randomProgram = new Random().nextInt(3);

    public Channel(String nameChannel) {
        this.nameChannel = nameChannel;
        this.program = new Program[MAX_PROGRAM_NUMBER];
    }

    //Получение названия канала
    public String getNameChannel() {
        return nameChannel;
    }

    //Метод добавления прогаммы в канал
    public void addPrograms(Program nameProgram) {
        if (this.countProgram < MAX_PROGRAM_NUMBER) {
            this.program[countProgram] = nameProgram;
            countProgram++;
        } else {
            System.err.println("Превышен предел количества программ в канале");
        }
    }

    //Метод вывода названия случайной программы
    public String getProgram() {
        return program[randomProgram].getNameProgram();
    }
}
