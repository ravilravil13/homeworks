package Lesson6_Fakhrutdinov;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        //Создаю обьект телевизор
        TV Televisor = new TV();

        //Создаю обьект пульт
        RemoteController remoteController = new RemoteController(Televisor);

        //Создаю два канала
        Channel chanel1 = new Channel("TNT");
        Channel chanel2 = new Channel("Friday");

        //Создаю программы
        Program program1 = new Program("Comedy Club");
        Program program2 = new Program("Comedy Women");
        Program program3 = new Program("Nasha rasha");
        Program program4 = new Program("Orel i reshka");
        Program program5 = new Program("Mir naiznanku");
        Program program6 = new Program("Na nozhakh");

        //Добавляю в 1-й канал программы (Метод добавления программы в канал)
        chanel1.addPrograms(program1);
        chanel1.addPrograms(program2);
        chanel1.addPrograms(program3);

        //Метод вывода названия случайной программы (показ программы) p.s Канал под номером 0
        System.out.println(chanel1.getProgram());

        //Добавляю во 2-й канал программы
        chanel2.addPrograms(program4);
        chanel2.addPrograms(program5);
        chanel2.addPrograms(program6);

        //Добавляю в Телевизор каналы (Метод добавления канала)
        Televisor.addChannels(chanel1);
        Televisor.addChannels(chanel2);

        //Метод обращения к каналу по его номеру в массиве (выводит название случайно программы канала)
        System.out.println(Televisor.getChannelsProgram(1));

        //Через пульт по номеру канала получаю случайную программу.
        //Метод обращения к определенному каналу телевизора под определенным номером
        System.out.println(remoteController.getChanel(1));
    }
}
