package ru.ravilgamedev.game.services;

import ru.ravilgamedev.game.models.Player;

import java.util.List;
import java.util.Map;

public interface GameService {
    /**
     * Метод запускает игру
     * @param players Мапа игроков, где ключ-ip, значение - nickname
     * @return id игры
     */
    Long startGame(Map<String,String> players);

    /**
     * Метод фиксирует попадание в игрока.
     *
     * @param gameID          В какой игре происходил выстрел.
     * @param shooterNickname Никнейм игрока, который стрелял.
     * @param targetNickname  Никнейм игрока, в которого попало.
     */
    void shoot(Long gameID, String shooterNickname, String targetNickname);
}
