package Lesson6_Fakhrutdinov;

public class RemoteController {
    private TV Televisor;

    public RemoteController(TV televisor) {
        this.Televisor = televisor;
    }

    public String getChanel(int number) {
        return Televisor.getChannelsProgram(number);
    }
}
