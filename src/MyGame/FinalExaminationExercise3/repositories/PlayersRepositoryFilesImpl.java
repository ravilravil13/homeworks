package MyGame.FinalExaminationExercise3.repositories;

import MyGame.FinalExaminationExercise3.models.Player;
import MyGame.FinalExaminationExercise3.models.PlayerBuilder;
import MyGame.FinalExaminationExercise3.utils.IdGenerator;

import java.io.*;
import java.util.*;
import java.util.function.Function;

@SuppressWarnings("unused")
public class PlayersRepositoryFilesImpl implements PlayersRepositoiry {

    private final String filename;
    private final IdGenerator idGenerator;
    //Перевод из строки в игрока
    Function<String, Player> lineToPlayer = line -> {
        String[] parts = line.split("\\|");
        String id = parts[0];
        String nickName = parts[1];
        String maxScore = parts[2];
        String countOfWin = parts[3];
        String countOfFail = parts[4];
        String ip = parts[5];
        return new PlayerBuilder()
                .setId(Integer.parseInt(id))
                .setLastIp(ip)
                .setNickName(nickName)
                .setMaxScore(Integer.parseInt(maxScore))
                .setCountOfWins(Integer.parseInt(countOfWin))
                .setCountOfFail(Integer.parseInt(countOfFail))
                .createPlayer();
    };

    //Создание репозитория.
    //Если файла не было, конструктор создаст файл.
    public PlayersRepositoryFilesImpl(String filename, IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
        this.filename = filename;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true))) {
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void save(Player player) {
        int lastId = idGenerator.getLastId();
        player.setId(lastId);
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, true))) {
                bufferedWriter.write(player.getId() + "|" +
                        player.getNickName() + "|" +
                        player.getMaxScore() + "|" +
                        player.getCountOfWins() + "|" +
                        player.getCountOfFail() + "|" +
                        player.getLastIp() + "\n");
            idGenerator.nextId();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Player player) {
        List<Player> playerList = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            String line = bufferedReader.readLine();
            for (int lineNumber = 0; line != null; lineNumber++) {
                Player currentPlayer = lineToPlayer.apply(line);
                playerList.add(currentPlayer);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();

            for (Player value : playerList) {
                if (value.getId().equals(player.getId())) {
                    playerList.set(player.getId() - 1, player);
                    try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, false))) {
                        for (Player currentPlayer : playerList) {
                            bufferedWriter.write(currentPlayer.getId() + "|" +
                                    currentPlayer.getNickName() + "|" +
                                    currentPlayer.getMaxScore() + "|" +
                                    currentPlayer.getCountOfWins() + "|" +
                                    currentPlayer.getCountOfFail() + "|" +
                                    currentPlayer.getLastIp() + "\n");
                        }
                    } catch (IOException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
            }
        } catch (
                IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Optional<Player> findByNickName(String nickname) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filename))) {
            String line = bufferedReader.readLine();
            for (int lineNumber = 0; line != null; lineNumber++) {
                Player currentPlayer = lineToPlayer.apply(line);
                if (currentPlayer.getNickName().equals(nickname)) {
                    return Optional.of(currentPlayer);
                } else {
                    line = bufferedReader.readLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return Optional.empty();
    }
}

