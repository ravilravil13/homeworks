package Lesson6_Fakhrutdinov;

public class TV {
    private final int MAX_CHANEL_NUMBER = 2;
    private int countChanel;
    //Массив каналов
    private Channel[] channels;
    private RemoteController remoteController;

    public TV() {
        this.channels = new Channel[MAX_CHANEL_NUMBER];
    }

    //Метод добавления канала
    public void addChannels(Channel nameChanel) {
        if (this.countChanel < MAX_CHANEL_NUMBER) {
            this.channels[countChanel] = nameChanel;
            countChanel++;
        } else {
            System.err.println("Превышен предел количества программ в канале");
        }
    }

    //Метод обращения к каналу по его номеру в массиве
    public String getChannelsProgram(int number) {
        return channels[number].getProgram();
    }
}
