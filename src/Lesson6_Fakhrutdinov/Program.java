package Lesson6_Fakhrutdinov;

public class Program {
    private String nameProgram;

    public Program(String nameProgram) {
        this.nameProgram = nameProgram;
    }

    //Получение название программы
    public String getNameProgram() {
        return nameProgram;
    }
}
