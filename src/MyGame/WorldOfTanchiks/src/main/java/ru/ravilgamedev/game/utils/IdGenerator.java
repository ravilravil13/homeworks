package ru.ravilgamedev.game.utils;

public interface IdGenerator {
    Integer nextId();
    Integer getLastId();
}
