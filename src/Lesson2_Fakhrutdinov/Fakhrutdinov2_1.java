package Lesson2_Fakhrutdinov;

import java.util.Scanner;
import java.util.Arrays;

class Fakhrutdinov2_1 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        for (int i = array.length - 1; i >= 0; i--) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));
    }
}
