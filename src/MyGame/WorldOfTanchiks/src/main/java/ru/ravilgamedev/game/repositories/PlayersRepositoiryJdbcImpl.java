package ru.ravilgamedev.game.repositories;

import ru.ravilgamedev.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class PlayersRepositoiryJdbcImpl implements PlayersRepositoiry<Player> {

    private final DataSource dataSource;

    public PlayersRepositoiryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Player player) {
        //Получаем подключение из dataSource.
        try (Connection connection = dataSource.getConnection()) {
            //language=sql
            String sql = "insert into players_repository(ip, nick_name) values (?,?)";
            //Расставляем в параметры поля игрока.
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, player.getIp());
            preparedStatement.setString(2, player.getNickName());
            if (preparedStatement.executeUpdate() > 0) {
                System.out.println("Сохранение игрока в players_repository под ником " + player.getNickName() + " прошло успешно");
            } else {
                System.out.println("Сохранение игрока в players_repository под ником " + player.getNickName() + " не удалось!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Player player) {
        //Получаем подключение из dataSource.
        try (Connection connection = dataSource.getConnection()) {
            //language=sql
            //Делаем запрос на пользователей и просим получить сгенерированные ключи.
            PreparedStatement preparedStatement = connection.prepareStatement("select * from players_repository");
            //Отправка запроса
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                //Если среди сгенерированных ключей совпал с ключом игрока.
                if (resultSet.getLong("player_id") == player.getId()) {
                    //language=sql
                    String sql = "update players_repository set ip=?, max_count=?, count_of_win=?, count_of_fails=? where player_id=?";
                    preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setString(1, player.getIp());
                    preparedStatement.setLong(2, player.getMaxCount());
                    preparedStatement.setLong(3, player.getCountOfWins());
                    preparedStatement.setLong(4, player.getCountOfFail());
                    preparedStatement.setLong(5, player.getId());
                    if (preparedStatement.executeUpdate() > 0) {
                        System.out.println("Игрок под ID = " + player.getId() + "в players_repository успешно обновлен!");
                        return;
                    } else {
                        System.out.println("Обновление игрока под ID = " + player.getId() + "в players_repository прошло неуспешно!");
                    }
                }
            }
            System.err.println("Такого игрока нет!!!");

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Optional<Player> findByNickName(String nickname) {
        try (Connection connection = dataSource.getConnection()) {
            //language=sql
            String sql = "select * from players_repository";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            return Optional.ofNullable(PlayerFromRepository(nickname, resultSet));
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Player getByNickName(String nickname) {
        try (Connection connection = dataSource.getConnection()) {
            //language=sql
            String sql = "select * from players_repository";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            return PlayerFromRepository(nickname, resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Player getById(Long playerId) {
        String sql = "select * from players_repository where player_id=?";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, playerId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Player.builder()
                        .id(playerId)
                        .ip(resultSet.getString("ip"))
                        .nickName(resultSet.getString("nick_name"))
                        .maxCount(resultSet.getInt("max_count"))
                        .countOfWins(resultSet.getInt("count_of_win"))
                        .countOfFail(resultSet.getInt("count_of_fails"))
                        .build();
            } else {
                throw new IllegalArgumentException("Запрос не выполнен, игрока c ID = " + playerId + " нету в players_repository");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Player PlayerFromRepository(String nickname, ResultSet resultSet) {
        try {
            while (resultSet.next()) {
                if (resultSet.getString("nick_name").equals(nickname)) {
                    return Player.builder()
                            .id(resultSet.getLong("player_id"))
                            .ip(resultSet.getString("ip"))
                            .nickName(resultSet.getString("nick_name"))
                            .maxCount(resultSet.getInt("max_count"))
                            .countOfWins(resultSet.getInt("count_of_win"))
                            .countOfFail(resultSet.getInt("count_of_fails"))
                            .build();
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }
}