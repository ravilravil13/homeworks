package Lesson10_Fakhrutdinov;

public interface Collection {

    void add(int element);

    boolean contains(int element);

    int size();

    void remove(int element);

    Iterator iterator();
}
