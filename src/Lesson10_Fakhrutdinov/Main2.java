package Lesson10_Fakhrutdinov;

public class Main2 {
    public static void main(String[] args) {
        List list = new ArrayList();
        for (int index = 0; index < 15; index++) {
            list.add(index);
        }

        Iterator iterator = list.iterator();

        System.out.println(list.contains(50));

        list.remove(9);

        list.removeByIndex(0);

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.err.println();

        System.out.println(list.get(9));
    }
}
