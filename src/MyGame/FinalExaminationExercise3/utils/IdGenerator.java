package MyGame.FinalExaminationExercise3.utils;

public interface IdGenerator {
    Integer nextId();
    Integer getLastId();
}
