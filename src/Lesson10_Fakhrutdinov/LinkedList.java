package Lesson10_Fakhrutdinov;

public class LinkedList implements List {

    private Node first;
    private Node last;
    private int size;

    public static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    public int get(int index) {
        if (index >= 0 && index < size) {
            Node current = first;

            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        System.err.println("Index out of bounds");
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < size) {
            Node current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            current.value = current.next.value;
            current.next = current.next.next;
            size--;
        } else {
            System.err.println("Индекс не в границах листа");
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        size++;
    }

    @Override
    public boolean contains(int element) {
        Node current = first;
        while (current != null) {
            if (current.value == element) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void remove(int element) {
        Node current = first;
        while (current != null) {
            if (current.value == element) {
                current.value = current.next.value;
                current.next = current.next.next;
                size--;
                return;
            }
            current = current.next;
        }
    }

    @Override
    public Iterator iterator() {
        return null;
    }
}
