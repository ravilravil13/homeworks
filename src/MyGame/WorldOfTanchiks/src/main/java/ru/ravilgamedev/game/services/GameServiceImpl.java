package ru.ravilgamedev.game.services;

import ru.ravilgamedev.game.models.Game;
import ru.ravilgamedev.game.models.Player;
import ru.ravilgamedev.game.models.Shoot;
import ru.ravilgamedev.game.repositories.GameRepository;
import ru.ravilgamedev.game.repositories.PlayersHitsRepository;
import ru.ravilgamedev.game.repositories.PlayersRepositoiry;
import ru.ravilgamedev.game.repositories.ShootRepository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class GameServiceImpl implements GameService {

    public GameServiceImpl(PlayersRepositoiry<Player> playersRepositoiry, GameRepository gameRepositories, ShootRepository shootRepository, PlayersHitsRepository playersHitsRepository) {
        this.playersRepositoiry = playersRepositoiry;
        this.gameRepositories = gameRepositories;
        this.shootRepository = shootRepository;
        this.playersHitsRepository = playersHitsRepository;
    }

    private final PlayersRepositoiry<Player> playersRepositoiry;
    private final GameRepository gameRepositories;
    private final ShootRepository shootRepository;
    private final PlayersHitsRepository playersHitsRepository;

    @Override
    public Long startGame(Map<String, String> players) {

        //Список игроков
        List<Player> playerList = new ArrayList<>();

        //Зарезервированная map "игроки-количество попаданий".
        Map<Player, Integer> playersHits = new HashMap<>();

        //Проверка игроков в базе данных.
        for (Map.Entry<String, String> entry : players.entrySet()) {
            Player checkedPlayer = checkIsExist(entry.getKey(), entry.getValue());
            playerList.add(checkedPlayer);
            playersHits.put(checkedPlayer, 0);
        }

        //Создаем игру
        Game game = Game.builder()
                .players(playerList)
                .playersHits(playersHits)
                .dateTime(LocalDateTime.now())
                .secondGameTimeAmount(LocalTime.MIN)
                .build();

        //Сохраняем игру
        gameRepositories.save(game);

        //Получаем из BD сгенерированный ID и кладем в Game.
        game.setId(gameRepositories.getId());
        return game.getId();
    }

    /**
     * Метод ищет игрока по никнейму, если такой игрок был -
     * метод меняет IP данного игрока.
     * <p>
     * Если игрока не было, метод создает нового и сохраняет его.
     *
     * @param ip       IP - игрока.
     * @param nickname Nickname - игрока
     * @return Возвращает игрока (Player).
     */
    private Player checkIsExist(String ip, String nickname) {
        Player result;
        Optional<Player> playerOptional = playersRepositoiry.findByNickName(nickname);
        // Если игрока под таким никнеймом нет
        // создаем нового игрока и сохраняем его.
        Player player;
        if (playerOptional.isEmpty()) {
            player = Player.builder()
                    .ip(ip)
                    .nickName(nickname)
                    .countOfFail(0)
                    .countOfWins(0)
                    .maxCount(0)
                    .build();
            playersRepositoiry.save(player);
        } else {
            //Если такой игрока есть, меняем его IP
            //И обновляем в репозитории.
            player = playerOptional.get();
            player.setIp(ip);
            playersRepositoiry.update(player);
        }
        result = player;
        return result;
    }

    @Override
    public void shoot(Long gameID, String shooterNickname, String targetNickname) {
        Map<Player, Integer> playersHitsCount;
        //Получаем информацию об игроках.
        Player shooter = playersRepositoiry.getByNickName(shooterNickname);
        Player target = playersRepositoiry.getByNickName(targetNickname);
        //Находим игру.
        Game game = gameRepositories.getById(gameID);
        //Создаем выстрел
        Shoot shoot = Shoot.builder()
                .shooter(shooter)
                .target(target)
                .timeOfShoot(LocalDateTime.now())
                .game(game)
                .build();
        //Увеличиваем количество попаданий в этой игре.
        //Делаем проверку, кто из игроков делал выстрел.
        playersHitsCount = game.getPlayersHits();
        for (Map.Entry<Player, Integer> entry : playersHitsCount.entrySet()) {
            if (entry.getKey().getId().equals(shooter.getId())) {
                game.getPlayersHits().put(shooter, game.getPlayersHits().get(shooter)+1);
            }
        }
        //Увеличиваем очки у того кто стрелял.
        shooter.setMaxCount(shooter.getMaxCount() + 1);
        //Обновляем данные по стрелку.
        playersRepositoiry.update(shooter);
        //Обновляем таблицу попаданий
        playersHitsRepository.update(game);
        //Обновляем данные по игре.
        gameRepositories.update(game);
        //Сохраняем выстрел.
        shootRepository.save(shoot);
    }


    }
