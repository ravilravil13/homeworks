package Lesson1_Fakhrutdinov;

public class lesson1_Fakhrutdinov_2 {
    public static void main(String[] args) {
        int number = 17000;
        int numberCopy = number;
        String binaryNumber = "";
        if (number > 65356) {
            numberCopy /= 65356;
            binaryNumber += "" + (numberCopy % 2);
        }
        if (number > 32768) {
            numberCopy /= 32768;
            binaryNumber += "" + (numberCopy % 2);
        }
        if (number > 16384) {
            numberCopy = number / 16384;
            binaryNumber += "" + (numberCopy % 2);
        }
        numberCopy = number / 8192;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 4096;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 2048;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 1024;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 512;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 256;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 128;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 64;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 32;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 16;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 8;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 4;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 2;
        binaryNumber += "" + (numberCopy % 2);
        numberCopy = number / 1;
        binaryNumber += "" + (numberCopy % 2);
        System.out.println(binaryNumber);
    }
}