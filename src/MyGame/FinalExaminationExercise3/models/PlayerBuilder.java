package MyGame.FinalExaminationExercise3.models;

public class PlayerBuilder {
    private Integer id;
    private String lastIp;
    private String nickName;
    private int maxScore;
    private int countOfWins;
    private int countOfFail;

    public PlayerBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public PlayerBuilder setLastIp(String lastIp) {
        this.lastIp = lastIp;
        return this;
    }

    public PlayerBuilder setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public PlayerBuilder setMaxScore(int maxScore) {
        this.maxScore = maxScore;
        return this;
    }

    public PlayerBuilder setCountOfWins(int countOfWins) {
        this.countOfWins = countOfWins;
        return this;
    }

    public PlayerBuilder setCountOfFail(int countOfFail) {
        this.countOfFail = countOfFail;
        return this;
    }

    public Player createPlayer() {
        return new Player(id, lastIp, nickName, maxScore, countOfWins, countOfFail);
    }
}